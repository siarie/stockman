<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Items extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Auth_model');
		if (!$this->Auth_model->is_logged_in()) redirect(base_url('auth/login'));

		$this->load->model('Item_model');
	}

	public function index()
	{
		$data = [
			'title' => 'Items',
			'page' => 'pages/items/index',
			'data' => [
				'items'=> $this->Item_model->get()
			]
		];

		$this->load->view('template', $data);
	}

	public function add()
	{
		$data = [
			'title' => 'Items',
			'page' => 'pages/items/add'
		];

		$this->load->view('template', $data);
	}

	public function do_add()
	{
		$data = [
			'code' => $this->input->post('code'),
			'name' => $this->input->post('name'),
			'stock' => $this->input->post('stock'),
			'price' => $this->input->post('price'),
		];

		if ($this->Item_model->getByCode($data['code'])) {
			$this->session->set_flashdata('error-code', 'Code is already exists.');
			redirect(base_url('items/add'));
		}

		try {
			$this->Item_model->add($data);
			$this->session->set_flashdata('success', 'Item successfully added.');
			redirect(base_url('items'));
		} catch (\Exception $e) {
			$this->session->set_flashdata('error', 'Something went wrong while inserting data.');
			redirect('items/add');
		}
	}

	public function edit($id)
	{
		$data = [
			'title' => 'Items',
			'page' => 'pages/items/edit',
			'data' => [
				'item' => $this->Item_model->getById($id)
			]
		];

		$this->load->view('template', $data);
	}

	public function update() {
		if ($this->input->server('REQUEST_METHOD') === 'POST') {
			$data = [
				'code' => $this->input->post('code'),
				'name' => $this->input->post('name'),
				'stock' => $this->input->post('stock'),
				'price' => $this->input->post('price'),
			];

			try {
				$this->Item_model->update($this->input->post('id'), $data);
				$this->session->set_flashdata('success', 'Item updated successfully.');
				redirect(base_url('items'));
			} catch (\Exception $e) {
				$this->session->set_flashdata('error', 'Something went wrong while update data.');
				redirect('items/add');
			}
		}

		show_404();
	}

	public function delete()
	{
		if ($this->input->server('REQUEST_METHOD') === 'POST') {
			try {
				$this->Item_model->delete($this->input->post('itemId'));
				$this->session->set_flashdata('success', 'Item successfully deleted.');
				redirect(base_url('items'));
			} catch (\Exception $e) {
				$this->session->set_flashdata('error', 'Something went wrong while removing data.');
				redirect('items');
			}
		}

		show_404();
	}
}
