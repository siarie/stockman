<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		show_404();
	}

	public function login()
	{
		if ($this->Auth_model->is_logged_in()) redirect(base_url('dashboard'));

		$this->form_validation->set_rules($this->Auth_model->rules);
		if ($this->form_validation->run()) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$this->load->model('User_model');

			$login = $this->Auth_model->login($username, $password);
			if (!$login) {
				$this->session->set_flashdata('error', 'Invalid credentials');
				redirect(base_url('/auth/login'));
			}

			redirect(base_url('/dashboard'));
		}

		$data = array(
			"title" => "Login",
		);

		$this->load->view('pages/auth/login', $data);
	}

	public function logout()
	{
		if ($this->input->server('REQUEST_METHOD') === 'POST') {
			$this->Auth_model->logout();
			redirect(base_url('/auth/login'));
		}

		show_404();
	}
}
