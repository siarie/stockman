<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model');
		if (!$this->Auth_model->is_logged_in()) redirect(base_url('auth/login'));
		if (!$this->Auth_model->is_logged_in())
			redirect(base_url('dashboard'));
	}

	public function index()
	{
		$this->load->model('User_model');
		$data = array(
			"title" => "User",
			"page" => "pages/user/index",
			"data" => [
				"users" => $this->User_model->get()
			]
		);
		$this->load->view('template', $data);
	}

	public function add()
	{
		$data = array(
			'title' => 'Add User',
			'page' => 'pages/user/add'
		);

		$this->load->view('template', $data);
	}

	public function do_add()
	{
		if ($this->input->server('REQUEST_METHOD') === 'POST') {
			$this->load->model('User_model');
			$this->load->library('form_validation');

			$password = password_hash($this->input->post('password'), PASSWORD_BCRYPT);

			$superadmin = $this->input->post('superadmin');

			$data = array(
				'username' => $this->input->post('username'),
				'password' => $password,
				'is_superadmin' => ($superadmin) ? true : false
			);

			try {
				$this->User_model->add($data);
				$this->session->set_flashdata('success', 'User successfully added');
				redirect(base_url('/user'));
			} catch (\Exception $err) {
				return $err->getMessage();
			};
		}


		show_404();
	}

	public function delete()
	{
		if ($this->input->server('REQUEST_METHOD') === 'POST') {
			$this->load->model('User_model');

			try {
				$this->User_model->delete($this->input->post('userId'));
				$this->session->set_flashdata('success', 'User successfully deleted.');
				redirect(base_url('user'));
			} catch (\Exception $e) {
				$this->session->set_flashdata('error', 'Something went wrong while removing data.');
				redirect('user');
			}
		}

		show_404();
	}
}
