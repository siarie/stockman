<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Auth_model');
		if (!$this->Auth_model->is_logged_in()) redirect(base_url('auth/login'));
	}

	public function index()
	{
		$data = [
			'title' => 'Dashboard',
			'page' => 'pages/dashboard/index'
		];

		$this->load->view('template', $data);
	}
}
