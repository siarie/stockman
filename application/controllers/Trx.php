<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trx extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Auth_model');
		if (!$this->Auth_model->is_logged_in()) redirect(base_url('auth/login'));

		$this->load->model('Trx_model');
	}

	public function index()
	{
		$data = [
			'title' => 'Transaction',
			'page' => 'pages/trx/index',
			'data' => [
				'trxin'=> $this->Trx_model->get('in'),
				'trxout'=> $this->Trx_model->get('out')
			]
		];

		$this->load->view('template', $data);
	}

	public function in()
	{
		$data = [
			'title' => 'Transaction',
			'page' => 'pages/trx/in',
			'transactions' => $this->Trx_model->get('in')
		];

		$this->load->view('template', $data);
	}

	public function out()
	{
		$data = [
			'title' => 'Transaction',
			'page' => 'pages/trx/out',
			'transactions' => $this->Trx_model->get('out')
		];

		$this->load->view('template', $data);
	}

	public function add()
	{
		$this->load->model('Item_model');
		$this->load->library('form_validation');

		$this->form_validation->set_rules($this->Trx_model->rules);
		if ($this->form_validation->run()) {
			$data = array(
				'item_id' => $this->input->post('item_id'),
				'trx_type' => $this->input->post('trx_type'),
				'qty' => $this->input->post('qty'),
			);

			try {
				if ($data['trx_type'] == "in") $this->Item_model->increase($data['item_id'], $data['qty']);
				if ($data['trx_type'] == "out") $this->Item_model->decrease($data['item_id'], $data['qty']); 

				$this->Trx_model->add($data);
				$this->session->set_flashdata('success', 'Data added succesfully.');
				redirect(base_url('trx'));
			} catch (\Exception $e) {
				$this->session->set_flashdata('error', $e->getMessage());
				redirect(base_url('trx/add'));
			}
		}

		$data = [
			'title' => 'Transaction',
			'page' => 'pages/trx/add',
			'data' => array(
				'items' => $this->Item_model->get()
			)
		];

		$this->load->view('template', $data);
	}
}
