<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth_model extends CI_Model
{
	private $_tablename = 'users';
	const SESSION_KEY = 'user';

	public $rules = array(
		[
			'field' => 'username',
			'label' => 'Username',
			'rules' => 'required',
		],
		[
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'required',
		]
	);

	public function login(string $username, string $password)
	{
		$this->db->select("username, password, is_superadmin");
		$this->db->from($this->_tablename);
		$this->db->where('username', $username);
		$user = $this->db->get()->row();

		if (!$user) return false;
		if (!password_verify($password, $user->password)) return false;

		$this->session->set_userdata([self::SESSION_KEY => $user]);

		return $this->session->has_userdata(self::SESSION_KEY);
	}

	public function is_logged_in()
	{
		if (!$this->session->has_userdata(self::SESSION_KEY))
			return false;

		return true;
	}

	public function is_superadmin()
	{
		if ($this->session->user->is_supperadmin === 't')
			return true;

		return false;
	}

	public function logout()
	{
		$this->session->unset_userdata(['user']);
	}
}
