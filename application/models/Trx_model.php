<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trx_model extends CI_Model
{
	private $_tablename = 'trx';
	private $_select = 'trx.id, trx.item_id, trx.trx_type, trx.qty, trx.created_at, items.name';

	public $rules = array(
		array(
			'field' => 'trx_type',
			'label' => 'Transaction Type',
			'rules' => 'required'
		),
		array(
			'field' => 'item_id',
			'label' => 'Items',
			'rules' => 'required'
		),
		array(
			'field' => 'qty',
			'label' => 'Quantity',
			'rules' => 'required|numeric'
		)
	);

	public function get(string $type = NULL)
	{
		$this->db->select($this->_select);
		if ($type !== NULL)
			$this->db->where('trx_type', $type);

		$this->db->join('items', 'items.id = trx.item_id', 'inner join');

		$query = $this->db->get($this->_tablename);
		return $query->result();
	}

	public function add(array $data)
	{	
		$this->db->insert($this->_tablename, $data);
	}
}
