<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
	private $_tablename = 'users';

	public function rules(): array
	{
		return array(
			[
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required|is_unique[users.username]',
			],
			[
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required',
			]
		);
	}

	public function get()
	{
		$this->db->select('id, username, is_superadmin, created_at');
		return $this->db->get($this->_tablename)->result();
	}

	public function add(array $data)
	{
		$this->db->insert($this->_tablename, $data);
	}

	public function login(string $username, string $password)
	{
		$this->db->select("username, password, is_superadmin");
		$this->db->from($this->_tablename);
		$this->db->where('username', $username);
		$user = $this->db->get()->row();

		if (!password_verify($password, $user->password)) return false;

		return $user;
	}

	public function delete(string $id)
	{
		$this->db->delete($this->_tablename, ['id' => $id]);
	}
}
