<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Item_model extends CI_Model
{
	private $_tablename = 'items';
	private $_select = 'id, code, name, stock, price';

	public function get()
	{
		$this->db->select($this->_select);
		return $this->db->get($this->_tablename)->result();
	}

	public function getByCode(string $code)
	{
		$this->db->select($this->_select);
		$this->db->from($this->_tablename);
		$this->db->where('code', $code);

		$item = $this->db->get();

		return ($item->row()) ? $item->row() : NULL;
	}

	public function getById(int $id)
	{
		$this->db->select($this->_select);
		$this->db->from($this->_tablename);
		$this->db->where('id', $id);

		$item = $this->db->get();

		return ($item->row()) ? $item->row() : NULL;
	}

	public function delete(int $id)
	{
		$this->db->delete($this->_tablename, ['id' => $id]);
	}

	public function add(array $data)
	{
		$this->db->insert($this->_tablename, $data);
	}

	public function update(int $id, array $data)
	{
		$this->db->where('id', $id);
		$this->db->update($this->_tablename, $data);
	}

	public function increase(int $id, int $qty)
	{
		if ($qty < 1) throw new Exception("Minimum quantity is 1", 1);

		$query = $this->db->where('id', $id);
		$item = $query->get($this->_tablename)->row();

		$lastStock = $item->stock;

		$query->update($this->_tablename, ['stock' => $lastStock + $qty]);
	}

	public function decrease(int $id, int $qty)
	{
		if ($qty < 1) throw new Exception("Minimum quantity is 1", 1);
		
		$query = $this->db->where('id', $id);
		$item = $query->get($this->_tablename)->row();

		$lastStock = $item->stock;

		$this->db->update($this->_tablename, ['stock' => $lastStock - $qty], 'id=' . $id);
	}
}
