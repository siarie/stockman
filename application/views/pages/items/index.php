<?php if ($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger" role="alert">
		<?= $this->session->flashdata('error'); ?>
	</div>
<?php endif; ?>

<?php if ($this->session->flashdata('success')) : ?>
	<div class="alert alert-success" role="alert">
		<?= $this->session->flashdata('success'); ?>
	</div>
<?php endif; ?>

<div class="card shadow mb-4">
	<div class="card-header py-3 d-flex align-items-center">
		<h6 class="m-0 font-weight-bold text-primary">List Item</h6>
		<a href="<?= base_url('/items/add'); ?>" class="btn btn-primary ml-auto">Add Item</a>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th style="width: 4%;">Code</th>
						<th>Name</th>
						<th>Stock</th>
						<th>Price</th>
						<th style="width: 10%;"></th>
					</tr>
				</thead>
				<tbody>
					<?php if (count($data["items"]) > 0) : ?>

						<?php foreach ($data["items"] as $item) : ?>
							<tr>
								<td><?= $item->code; ?></td>
								<td><?= $item->name; ?></td>
								<td><?= $item->stock; ?></td>
								<td><?= $item->price; ?></td>
								<td class="text-center">
									<a href="<?= base_url('items/edit/' . $item->id) ?>" class="btn btn-warning btn-icon btn-sm">
										<i class="fas fa-edit"></i>
									</a>

									<button class="btn btn-danger btn-icon btn-sm" onclick="openDeleteModal(<?= $item->id ?>)">
										<i class="fas fa-trash"></i>
									</button>
								</td>
							</tr>
						<?php endforeach; ?>
					<?php else : ?>
						<tr>
							<td colspan="5" class="text-center">No items found.</td>
						</tr>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal" tabindex="-1" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Delete Item</h5>
				<button type="button" class="btn-close" onclick="closeDeleteModal()" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<p>Are you sure want to delete this item?</p>
			</div>
			<form class="modal-footer" action="<?= base_url('items/delete') ?>" method="POST">
				<input type="hidden" class="itemId" name="itemId">
				<button type="button" class="btn btn-secondary" onclick="closeDeleteModal()">Close</button>
				<button type="submit" class="btn btn-danger">Delete</button>
			</form>
		</div>
	</div>
</div>


<?php function append_scripts()
{
	echo <<<HTML
<script>
	var modalEl = document.getElementById('deleteModal')
	var itemID = modalEl.getElementsByClassName('itemId')[0]
	var deleteModal = new bootstrap.Modal(modalEl, {})

	function openDeleteModal(id) {
		itemID.value = id
		deleteModal.toggle()
	}

	function closeDeleteModal() {
		itemID.value = ""
		deleteModal.toggle()
	}
</script>
HTML;
}
?>
