<!-- DataTales Example -->
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Add item</h6>
	</div>
	<form action="<?= base_url('/items/do_add') ?>" method="POST">
		<div class="card-body">

			<div class="mb-3">
				<label for="code" class="form-label">Code</label>
				<input type="text" class="form-control <?php if ($this->session->flashdata('error-code')) echo 'is-invalid'; ?>" id="code" name="code" placeholder="Code...">
				<?php if ($this->session->flashdata('error-code')): ?>
				<div class="text-danger" role="alert">
					<?=$this->session->flashdata('error-code'); ?>
				</div>
				<?php endif; ?>
			</div>
			<div class="mb-3">
				<label for="name" class="form-label">Name</label>
				<input type="text" class="form-control" name="name" id="name" placeholder="Item Name..." />
			</div>

			<div class="mb-3">
				<label for="Stock" class="form-label">Stock</label>
				<input type="number" class="form-control" name="stock" id="stock" placeholder="Stock..." min="0" value="0" />
			</div>

			<div class="mb-3">
				<label for="price" class="form-label">Price</label>
				<input type="number" class="form-control" name="price" id="price" placeholder="Price..." min="0" value="0" />
			</div>

		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary mr-auto">Submit</button>
			<button type="button" class="btn btn-light" onclick="history.back()">Cancel</button>
		</div>
	</form>
</div>
