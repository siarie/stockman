<?php if ($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger" role="alert">
		<?= $this->session->flashdata('error'); ?>
	</div>
<?php endif; ?>

<?php if ($this->session->flashdata('success')) : ?>
	<div class="alert alert-success" role="alert">
		<?= $this->session->flashdata('success'); ?>
	</div>
<?php endif; ?>

<div class="card shadow mb-4">
	<div class="card-header py-3 d-flex align-items-center">
		<h6 class="m-0 font-weight-bold text-primary">List transaction</h6>
		<a href="<?= base_url('/trx/add'); ?>" class="btn btn-primary ml-auto">Add</a>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th style="width: 4%;"></th>
						<th>Date</th>
						<th>Name</th>
						<th>Qty</th>
					</tr>
				</thead>
				<tbody>
					<?php if (count($transactions) > 0) : ?>
						<?php foreach ($transactions as $trx) : ?>
							<tr>
								<td>
									<i class="fas fa-arrow-up text-danger"></i>
								</td>
								<td><?= date("d/m/Y", strtotime($trx->created_at));; ?></td>
								<td><?= $trx->name; ?></td>
								<td><?= $trx->qty; ?></td>
							</tr>
						<?php endforeach; ?>
					<?php else : ?>
						<tr>
							<td colspan="5" class="text-center">No items found.</td>
						</tr>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
