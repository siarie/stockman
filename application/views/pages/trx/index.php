<?php if ($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger" role="alert">
		<?= $this->session->flashdata('error'); ?>
	</div>
<?php endif; ?>

<?php if ($this->session->flashdata('success')) : ?>
	<div class="alert alert-success" role="alert">
		<?= $this->session->flashdata('success'); ?>
	</div>
<?php endif; ?>

<a href="<?= base_url('trx/add') ?>" class="btn btn-primary">
	New Transaction
</a>

<hr />

<div class="row">
	<div class="col-md-6">
		<div class="card shadow mb-4">
			<div class="card-header py-3 bg-primary">
				<h6 class="m-0 font-weight-bold text-white">
					<i class="fas fa-arrow-down"></i>
					Last transaction
				</h6>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th style="width: 4%;">Date</th>
								<th>Item Name</th>
								<th class="text-right">Qty</th>
							</tr>
						</thead>
						<tbody>
							<?php if (count($data["trxin"]) > 0) : ?>

								<?php foreach ($data["trxin"] as $trx) : ?>
									<tr>
										<td><?= date("d/m/Y", strtotime($trx->created_at)); ?></td>
										<td><?= $trx->name; ?></td>
										<td class="text-right"><?= $trx->qty; ?></td>
									</tr>
								<?php endforeach; ?>
							<?php else : ?>
								<tr>
									<td colspan="3" class="text-center">No items found.</td>
								</tr>
							<?php endif; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="card shadow mb-4">
			<div class="card-header py-3 bg-danger ">
				<h6 class="m-0 font-weight-bold text-white">
					<i class="fas fa-arrow-up"></i>
					Last transaction
				</h6>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th style="width: 4%;">Date</th>
								<th>Item Name</th>
								<th class="text-right">Qty</th>
							</tr>
						</thead>
						<tbody>
							<?php if (count($data["trxout"]) > 0) : ?>

								<?php foreach ($data["trxout"] as $trx) : ?>
									<tr>
										<td><?= date("d/m/Y", strtotime($trx->created_at)); ?></td>
										<td><?= $trx->name; ?></td>
										<td class="text-right"><?= $trx->qty; ?></td>
									</tr>
								<?php endforeach; ?>
							<?php else : ?>
								<tr>
									<td colspan="3" class="text-center">No items found.</td>
								</tr>
							<?php endif; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
