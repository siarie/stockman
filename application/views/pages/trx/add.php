<!-- DataTales Example -->
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Add transaction</h6>
	</div>
	<form action="<?= base_url('/trx/add') ?>" method="POST">
		<div class="card-body">
			<div class="mb-3">
				<div class="form-check form-check-inline">
					<input type="radio" class="form-check-input" id="btn-check-in" name="trx_type" value="in">
					<label class="form-check-label" for="btn-check-in" title="Barang Masuk">In</label>
				</div>
				<div class="form-check form-check-inline">
					<input type="radio" class="form-check-input" id="btn-check-out" name="trx_type" value="out">
					<label class="form-check-label" for="btn-check-out" title="Barang Keluar">Out</label>
				</div>
			</div>
			<div class="mb-3">
				<label for="code" class="form-label">Select Item</label>
				<select class="form-control" name="item_id">
					<option>Select Item</option>
					<?php if ($data['items']): foreach($data['items'] as $item): ?>
					<option value="<?=$item->id?>"><?=$item->name?></option>
					<?php endforeach; endif ?>
				</select>
				<?php if ($this->session->flashdata('error-code')) : ?>
					<div class="text-danger" role="alert">
						<?= $this->session->flashdata('error-code'); ?>
					</div>
				<?php endif; ?>
			</div>

			<div class="mb-3">
				<label for="qty" class="form-label">Quantity</label>
				<input type="number" class="form-control" name="qty" id="qty" placeholder="Quantity..." min="1" value="1" />
			</div>

		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary mr-auto">Submit</button>
			<button type="button" class="btn btn-light" onclick="history.back()">Cancel</button>
		</div>
	</form>
</div>
