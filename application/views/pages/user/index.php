<!-- DataTales Example -->
<div class="card shadow mb-4">
	<div class="card-header py-3 d-flex align-items-center">
		<h6 class="m-0 font-weight-bold text-primary">List Users</h6>
		<a href="<?= base_url('/user/add'); ?>" class="btn btn-primary ml-auto">Add User</a>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th style="width: 4%;">No</th>
						<th>User</th>
						<th>Role</th>
						<th style="width: 10%;"></th>
					</tr>
				</thead>
				<tbody>
					<?php if (count($data["users"]) > 0) : ?>

						<?php foreach ($data["users"] as $key => $user) : ?>
							<tr>
								<td><?= $key + 1; ?></td>
								<td><?= $user->username; ?></td>
								<td><?= ($user->is_superadmin === "t") ? 'superadmin' : 'staff'; ?></td>
								<td class="text-center">
									<a href="<?= base_url('user/edit/' . $user->id) ?>" class="btn btn-warning btn-icon btn-sm">
										<i class="fas fa-edit"></i>
									</a>

									<button class="btn btn-danger btn-icon btn-sm" onclick="openDeleteModal('<?= $user->id ?>')">
										<i class="fas fa-trash"></i>
									</button>
								</td>
							</tr>
						<?php endforeach; ?>
					<?php else : ?>
						<tr>
							<td colspan="4" class="text-center">No users found.</td>
						</tr>
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>


<div class="modal" tabindex="-1" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Delete User</h5>
				<button type="button" class="btn-close" onclick="closeDeleteModal()" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<p>Are you sure want to delete this user?</p>
			</div>
			<form class="modal-footer" action="<?= base_url('user/delete') ?>" method="POST">
				<input type="hidden" class="userId" name="userId">
				<button type="button" class="btn btn-secondary" onclick="closeDeleteModal()">Close</button>
				<button type="submit" class="btn btn-danger">Delete</button>
			</form>
		</div>
	</div>
</div>


<?php function append_scripts()
{
	echo <<<HTML
<script>
	var modalEl = document.getElementById('deleteModal')
	var userId = modalEl.getElementsByClassName('userId')[0]
	var deleteModal = new bootstrap.Modal(modalEl, {})

	function openDeleteModal(id) {
		userId.value = id
		deleteModal.toggle()
	}

	function closeDeleteModal() {
		userId.value = ""
		deleteModal.toggle()
	}
</script>
HTML;
}
?>
