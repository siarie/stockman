<!-- DataTales Example -->
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Add User</h6>
	</div>
	<form action="<?= base_url('/user/do_add') ?>" method="POST">
		<div class="card-body">

			<div class="mb-3">
				<label for="username" class="form-label">Email address</label>
				<input type="text" class="form-control" id="username" name="username" placeholder="Username...">
			</div>
			<div class="mb-3">
				<label for="password" class="form-label">Pasword</label>
				<input type="password" class="form-control" name="password" id="password" placeholder="Password..."/>
			</div>

			<div class="form-check">
				<input class="form-check-input" type="checkbox" name="superadmin" id="superadmin">
				<label class="form-check-label" for="superadmin">
					Super Admin?
				</label>
			</div>
		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-primary mr-auto">Submit</button>
			<button type="button" class="btn btn-light" onclick="history.back()">Cancel</button>
		</div>
	</form>
</div>
