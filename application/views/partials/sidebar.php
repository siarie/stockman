<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

	<!-- Sidebar - Brand -->
	<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
		<div class="sidebar-brand-icon rotate-n-15">
			<i class="fas fa-laugh-wink"></i>
		</div>
		<div class="sidebar-brand-text mx-3">Stockman</div>
	</a>

	<!-- Divider -->
	<hr class="sidebar-divider my-0">

	<!-- Nav Item - Dashboard -->
	<li class="nav-item <?= ($this->uri->segment(1) == "dashboard" || $this->uri->segment(1) == "") ? 'active' : '' ?>">
		<a class="nav-link" href="<?= base_url() ?>">
			<i class="fas fa-fw fa-tachometer-alt"></i>
			<span>Dashboard</span></a>
	</li>

	<!-- Nav Item - Dashboard -->
	<li class="nav-item <?= ($this->uri->segment(1) == "items") ? 'active' : '' ?>">
		<a class="nav-link" href="<?= base_url('items') ?>">
			<i class="fas fa-fw fa-box"></i>
			<span>Items</span></a>
	</li>

	<li class="nav-item <?= ($this->uri->segment(1) == "trx") ? 'active' : '' ?>">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			<i class="fas fa-fw fa-exchange-alt"></i>
			<span>Transaction</span>
		</a>
		<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<a class="collapse-item" href="<?=base_url('trx')?>">Index</a>
				<a class="collapse-item" href="<?=base_url('trx/in')?>">Stock In</a>
				<a class="collapse-item" href="<?=base_url('trx/out')?>">Stock Out</a>
			</div>
		</div>
	</li>

	<?php if ($_SESSION['user']->is_superadmin === 't') : ?>
		<li class="nav-item <?= ($this->uri->segment(1) == "user") ? 'active' : '' ?>">
			<a class="nav-link" href="<?= base_url('user') ?>">
				<i class="fas fa-fw fa-users"></i>
				<span>Users</span></a>
		</li>
	<?php endif; ?>

	<!-- Divider -->
	<hr class="sidebar-divider d-none d-md-block">

	<!-- Sidebar Toggler (Sidebar) -->
	<div class="text-center d-none d-md-inline">
		<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>

</ul>
<!-- End of Sidebar -->
